# AMC-Cloud：自动化Mock组件服务项目

## 介绍

AMC-Cloud(Automated Mock Component Cloud)：自动化Mock组件服务项目，用来对外暴露接口提供服务。

## 说明

- AMC的文档已经很完备了
- 具体的文档参见：https://gitee.com/loopstack/amc