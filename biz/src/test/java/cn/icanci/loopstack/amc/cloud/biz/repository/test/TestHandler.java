package cn.icanci.loopstack.amc.cloud.biz.repository.test;

import cn.hutool.json.JSONUtil;
import cn.icanci.loopstack.amc.spi.mock.MockCallStandardHandler;

/**
 * 测试TestHandler处理器
 * 
 * @author icanci
 * @since 1.0 Created in 2023/01/17 18:48
 */
public class TestHandler extends MockCallStandardHandler {

    @Override
    public Object execute(Object request) throws Exception {
        QuotaRequest bean = JSONUtil.toBean(String.valueOf(request), QuotaRequest.class);
        Quota quota = new Quota();
        quota.setMethod(bean.getUuid());
        quota.setResourceType(bean.getUuid());
        quota.setSource(bean.getUuid());
        quota.setPrepayValidateCount(bean.getUuid());
        quota.setBookCount(bean.getUuid());
        quota.setConversionRate(System.currentTimeMillis());
        return quota;
    }

    public static class QuotaRequest {
        private String uuid;

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }
    }

    public static class Quota {
        String method = "ALL";
        String resourceType;
        String source;
        String prepayValidateCount;
        String bookCount;
        long   conversionRate;

        public String getMethod() {
            return method;
        }

        public void setMethod(String method) {
            this.method = method;
        }

        public String getResourceType() {
            return resourceType;
        }

        public void setResourceType(String resourceType) {
            this.resourceType = resourceType;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getPrepayValidateCount() {
            return prepayValidateCount;
        }

        public void setPrepayValidateCount(String prepayValidateCount) {
            this.prepayValidateCount = prepayValidateCount;
        }

        public String getBookCount() {
            return bookCount;
        }

        public void setBookCount(String bookCount) {
            this.bookCount = bookCount;
        }

        public long getConversionRate() {
            return conversionRate;
        }

        public void setConversionRate(long conversionRate) {
            this.conversionRate = conversionRate;
        }
    }
}

// Tips 脚本内容
// import cn.hutool.json.JSONUtil;
// import cn.icanci.loopstack.amc.spi.mock.MockCallStandardHandler;
//
// public class TestHandler extends MockCallStandardHandler {
//å
//    @Override
//    public Object execute(Object request) throws Exception {
//        QuotaRequest bean = JSONUtil.toBean(String.valueOf(request), QuotaRequest.class);
//        Quota quota = new Quota();
//        quota.setMethod(bean.getUuid());
//        quota.setResourceType(bean.getUuid());
//        quota.setSource(bean.getUuid());
//        quota.setPrepayValidateCount(bean.getUuid());
//        quota.setBookCount(bean.getUuid());
//        quota.setConversionRate(System.currentTimeMillis());
//        return quota;
//    }
//
//    public static class QuotaRequest {
//        private String uuid;
//
//        public String getUuid() {
//            return uuid;
//        }
//
//        public void setUuid(String uuid) {
//            this.uuid = uuid;
//        }
//    }
//
//    public static class Quota {
//        String method = "ALL";
//        String resourceType;
//        String source;
//        String prepayValidateCount;
//        String bookCount;
//        long   conversionRate;
//
//        public String getMethod() {
//            return method;
//        }
//
//        public void setMethod(String method) {
//            this.method = method;
//        }
//
//        public String getResourceType() {
//            return resourceType;
//        }
//
//        public void setResourceType(String resourceType) {
//            this.resourceType = resourceType;
//        }
//
//        public String getSource() {
//            return source;
//        }
//
//        public void setSource(String source) {
//            this.source = source;
//        }
//
//        public String getPrepayValidateCount() {
//            return prepayValidateCount;
//        }
//
//        public void setPrepayValidateCount(String prepayValidateCount) {
//            this.prepayValidateCount = prepayValidateCount;
//        }
//
//        public String getBookCount() {
//            return bookCount;
//        }
//
//        public void setBookCount(String bookCount) {
//            this.bookCount = bookCount;
//        }
//
//        public long getConversionRate() {
//            return conversionRate;
//        }
//
//        public void setConversionRate(long conversionRate) {
//            this.conversionRate = conversionRate;
//        }
//    }
//}