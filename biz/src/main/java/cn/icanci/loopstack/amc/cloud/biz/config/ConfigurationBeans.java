package cn.icanci.loopstack.amc.cloud.biz.config;

import cn.icanci.loopstack.lsi.event.DefaultEventDispatcher;
import cn.icanci.loopstack.lsi.event.EventDispatcher;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author icanci(1205068)
 * @version Id: Config, v 0.1 2022/12/24 20:22 icanci Exp $
 */
@Configuration
public class ConfigurationBeans {
    /**
     * 事件分发器
     * 
     * @return 返回事件分发器
     */
    @Bean(name = "eventDispatcher")
    public EventDispatcher eventDispatcher() {
        return new DefaultEventDispatcher();
    }
}