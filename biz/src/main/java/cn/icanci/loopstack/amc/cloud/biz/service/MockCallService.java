package cn.icanci.loopstack.amc.cloud.biz.service;

import cn.icanci.loopstack.amc.cloud.common.model.MockCallWrapper;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/16 15:52
 */
public interface MockCallService {
    /**
     * mockCall
     * 
     * @param mockCallUuid mockCallUuid
     * @param mockRequest mockRequest
     * @return 执行结果包装器
     */
    MockCallWrapper mockCall(String mockCallUuid, String mockRequest);
}
