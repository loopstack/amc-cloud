package cn.icanci.loopstack.amc.cloud.biz.event.call;

import cn.icanci.loopstack.amc.cloud.biz.event.AmcCloudEvent;
import cn.icanci.loopstack.amc.cloud.common.model.MockCallWrapper;

import org.springframework.stereotype.Component;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/16 20:09
 */
@Component
public class MockCallEvent extends AmcCloudEvent {
    /**
     * Mock 请求uuid
     */
    private String          mockCallUuid;
    /**
     * mock请求参数
     */
    private String          mockCallRequest;
    /**
     * 执行时间
     */
    private long            runtime;
    /**
     * Mock请求Wrapper
     */
    private MockCallWrapper wrapper;

    public MockCallEvent() {
    }

    public MockCallEvent(String mockCallUuid, String mockCallRequest, long runtime, MockCallWrapper wrapper) {
        this.mockCallUuid = mockCallUuid;
        this.mockCallRequest = mockCallRequest;
        this.runtime = runtime;
        this.wrapper = wrapper;
    }

    public String getMockCallRequest() {
        return mockCallRequest;
    }

    public void setMockCallRequest(String mockCallRequest) {
        this.mockCallRequest = mockCallRequest;
    }

    public String getMockCallUuid() {
        return mockCallUuid;
    }

    public void setMockCallUuid(String mockCallUuid) {
        this.mockCallUuid = mockCallUuid;
    }

    public long getRuntime() {
        return runtime;
    }

    public void setRuntime(long runtime) {
        this.runtime = runtime;
    }

    public MockCallWrapper getWrapper() {
        return wrapper;
    }

    public void setWrapper(MockCallWrapper wrapper) {
        this.wrapper = wrapper;
    }
}
