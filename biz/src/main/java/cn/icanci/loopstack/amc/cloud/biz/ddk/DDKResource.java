package cn.icanci.loopstack.amc.cloud.biz.ddk;

import cn.icanci.loopstack.amc.cloud.biz.repository.AmcMockCallRepository;
import cn.icanci.loopstack.ddk.common.anno.DdkAfterCall;
import cn.icanci.loopstack.ddk.common.anno.DdkBeforeCall;
import cn.icanci.loopstack.ddk.common.anno.DdkCall;
import cn.icanci.loopstack.ddk.common.anno.DdkResource;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/16 15:34
 */
@Service
@DdkResource("#mock.cloud.notice")
public class DDKResource {

    private final Logger          logger = LoggerFactory.getLogger(DDKResource.class);

    @Resource
    private AmcMockCallRepository amcMockCallRepository;

    @DdkBeforeCall
    private synchronized void ddkBeforeCall(String mockCallUuid) {
        // No Op
    }

    @DdkCall
    private synchronized void ddkCall(String mockCallUuid) {
        logger.info("[DDKResource][ddkCall] mockCallUuid:{}, now is Start", mockCallUuid);
        amcMockCallRepository.refresh(mockCallUuid);
        logger.info("[DDKResource][ddkCall] mockCallUuid:{}, now is End", mockCallUuid);
    }

    @DdkAfterCall
    private synchronized void ddkAfterCall(String mockCallUuid) {
        // No Op
    }
}
