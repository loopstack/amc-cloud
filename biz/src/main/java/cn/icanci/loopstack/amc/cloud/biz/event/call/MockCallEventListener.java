package cn.icanci.loopstack.amc.cloud.biz.event.call;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.icanci.loopstack.amc.cloud.biz.service.AppMockTestService;
import cn.icanci.loopstack.lsi.event.BaseEventListener;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/16 20:10
 */
@Service
public class MockCallEventListener extends BaseEventListener<MockCallEvent> {

    @Resource
    private AppMockTestService appMockTestService;

    @Override
    protected void event(MockCallEvent event) {
        appMockTestService.log(event.getMockCallUuid(), event.getMockCallRequest(), event.getRuntime(), event.getWrapper());
    }

    @Override
    protected boolean isSupport(MockCallEvent event) {
        // 确定标准
        return event.getWrapper() != null;
    }
}
