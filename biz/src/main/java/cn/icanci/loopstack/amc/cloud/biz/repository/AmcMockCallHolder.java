package cn.icanci.loopstack.amc.cloud.biz.repository;

import cn.icanci.loopstack.amc.spi.mock.MockCallStandardHandler;
import cn.icanci.loopstack.script.enums.ScriptTypeEnum;

import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.ScriptEngine;

/**
 * 脚本执行数据持有者
 * - 如果是mvel2.0脚本，则不会有handlerClass和instance
 * - 如果是groovy脚本
 *   - 如果不是 {cn.icanci.loopstack.amc.spi.mock.MockCallStandardHandler}的实现类，则不会有handlerClass和instance
 *   - 如果是 {cn.icanci.loopstack.amc.spi.mock.MockCallStandardHandler}的实现类，则有handlerClass和instance
 *   
 * @author icanci
 * @since 1.0 Created in 2023/01/16 15:44
 */
public class AmcMockCallHolder {
    /**
     * mock uuid
     */
    private String                         mockCallUuid;
    /**
     * mock uuid
     */
    private String                         mockCallName;
    /**
     * 脚本类型
     */
    private ScriptTypeEnum                 scriptType;
    /**
     * 脚本内容
     */
    private String                         scriptContent;
    /**
     * 脚本执行引擎
     */
    private ScriptEngine                   scriptEngine;
    /**
     * 脚本执行引擎编译器，如果脚本执行引擎实现了Compilable
     * 那么 scriptEngine 和 compilable 则是同一个对象
     */
    private Compilable                     compilable;
    /**
     * 通过 Compilable 编译 scriptContent 之后的实例对象
     */
    private CompiledScript                 compiledScript;
    /**
     * 处理器groovy脚本Invoke类型
     */
    private Class<MockCallStandardHandler> handlerClass;
    /**
     * handlerClass 实例化对象
     */
    private MockCallStandardHandler        instance;

    public String getMockCallUuid() {
        return mockCallUuid;
    }

    public void setMockCallUuid(String mockCallUuid) {
        this.mockCallUuid = mockCallUuid;
    }

    public String getMockCallName() {
        return mockCallName;
    }

    public void setMockCallName(String mockCallName) {
        this.mockCallName = mockCallName;
    }

    public ScriptTypeEnum getScriptType() {
        return scriptType;
    }

    public void setScriptType(ScriptTypeEnum scriptType) {
        this.scriptType = scriptType;
    }

    public String getScriptContent() {
        return scriptContent;
    }

    public void setScriptContent(String scriptContent) {
        this.scriptContent = scriptContent;
    }

    public ScriptEngine getScriptEngine() {
        return scriptEngine;
    }

    public void setScriptEngine(ScriptEngine scriptEngine) {
        this.scriptEngine = scriptEngine;
    }

    public Compilable getCompilable() {
        return compilable;
    }

    public void setCompilable(Compilable compilable) {
        this.compilable = compilable;
    }

    public CompiledScript getCompiledScript() {
        return compiledScript;
    }

    public void setCompiledScript(CompiledScript compiledScript) {
        this.compiledScript = compiledScript;
    }

    public Class<MockCallStandardHandler> getHandlerClass() {
        return handlerClass;
    }

    public void setHandlerClass(Class<MockCallStandardHandler> handlerClass) {
        this.handlerClass = handlerClass;
    }

    public MockCallStandardHandler getInstance() {
        return instance;
    }

    public void setInstance(MockCallStandardHandler instance) {
        this.instance = instance;
    }
}
