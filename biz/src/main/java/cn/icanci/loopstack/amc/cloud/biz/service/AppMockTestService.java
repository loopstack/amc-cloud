package cn.icanci.loopstack.amc.cloud.biz.service;

import cn.icanci.loopstack.amc.cloud.common.model.MockCallWrapper;

/**
 * 记录执行日志
 * 
 * @author icanci
 * @since 1.0 Created in 2023/01/17 22:50
 */
public interface AppMockTestService {

    /**
     * 记录操作轨迹日志
     * 
     * @param mockUuid mockUuid
     * @param request 执行请求
     * @param runtime 方法执行时间
     * @param wrapper Wrapper对象
     */
    void log(String mockUuid, String request, long runtime, MockCallWrapper wrapper);
}
