package cn.icanci.loopstack.amc.cloud.biz.service.impl;

import cn.hutool.json.JSONUtil;
import cn.icanci.loopstack.amc.cloud.biz.service.AppMockTestService;
import cn.icanci.loopstack.amc.cloud.common.model.MockCallWrapper;
import cn.icanci.loopstack.amc.cloud.dal.mongodb.daointerface.AppMockCallReportDAO;
import cn.icanci.loopstack.amc.cloud.dal.mongodb.dataobject.AppMockCallReportDO;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/17 22:57
 */
@Service
public class AppMockTestServiceImpl implements AppMockTestService {
    private static final Logger  logger = LoggerFactory.getLogger(AppMockTestServiceImpl.class);

    @Resource
    private AppMockCallReportDAO appMockCallReportDAO;

    @Override
    public void log(String mockUuid, String request, long runtime, MockCallWrapper wrapper) {
        logger.info("[AppMockTestService][log] mockUuid:{},request:{},wrapper:{}", mockUuid, request, JSONUtil.toJsonStr(wrapper));

        AppMockCallReportDO report = new AppMockCallReportDO();
        report.setSuccess(wrapper.isSuccess());
        report.setMockName(wrapper.getMockName());
        report.setMockUuid(wrapper.getMockUuid());
        report.setMockRequest(wrapper.getMockRequest());
        report.setRuntime(runtime);
        report.setMockErrorMessage(wrapper.getMockErrorMessage());
        // hutool 的包方法，null值吃掉了，此处需要展示null值
        report.setMockResponse(JSON.toJSONString(wrapper.getMockResponse(), SerializerFeature.WriteMapNullValue));
        appMockCallReportDAO.insert(report);
    }
}
