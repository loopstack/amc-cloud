package cn.icanci.loopstack.amc.cloud.biz.exception;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/16 20:33
 */
public class MockCallScriptException extends RuntimeException {
    public MockCallScriptException() {
        super();
    }

    public MockCallScriptException(String message) {
        super(message);
    }

    public MockCallScriptException(String message, Throwable cause) {
        super(message, cause);
    }

    public MockCallScriptException(Throwable cause) {
        super(cause);
    }

    protected MockCallScriptException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
