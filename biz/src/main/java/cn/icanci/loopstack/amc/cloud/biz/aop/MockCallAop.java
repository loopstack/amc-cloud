package cn.icanci.loopstack.amc.cloud.biz.aop;

import cn.icanci.loopstack.amc.cloud.biz.event.call.MockCallEvent;
import cn.icanci.loopstack.amc.cloud.common.model.MockCallWrapper;
import cn.icanci.loopstack.lsi.event.EventDispatcher;

import javax.annotation.Resource;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * AOP拦截
 * 
 * @author icanci
 * @since 1.0 Created in 2023/01/16 19:56
 */
@Aspect
@Component
public class MockCallAop {
    @Resource
    private EventDispatcher eventDispatcher;

    @Pointcut("execution(public * cn.icanci.loopstack.amc.cloud.biz.service.impl.MockCallServiceImpl.mockCall(..))")
    private void callAop() {

    }

    @Around("callAop()")
    public Object doBefore(ProceedingJoinPoint pjp) throws Throwable {
        Object[] args = pjp.getArgs();
        long startTime = System.currentTimeMillis();
        // invoke
        Object returnVal = pjp.proceed();
        long endTime = System.currentTimeMillis();

        eventDispatcher.fire(new MockCallEvent(String.valueOf(args[0]), String.valueOf(args[1]), (endTime - startTime), (MockCallWrapper) returnVal), false);

        return returnVal;
    }
}
