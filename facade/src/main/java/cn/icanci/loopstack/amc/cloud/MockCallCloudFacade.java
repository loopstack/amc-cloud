package cn.icanci.loopstack.amc.cloud;

import cn.hutool.json.JSONUtil;
import cn.icanci.loopstack.amc.cloud.common.model.MockCallWrapper;
import cn.icanci.loopstack.amc.cloud.biz.service.MockCallService;
import cn.icanci.loopstack.amc.cloud.common.request.MockCallRequest;
import cn.icanci.loopstack.amc.cloud.common.response.MockCallResponse;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/16 14:31
 */
@RestController
@RequestMapping("mock/call")
public class MockCallCloudFacade {
    private static final Logger logger = LoggerFactory.getLogger(MockCallCloudFacade.class);
    @Resource
    private MockCallService     mockCallService;

    @PostMapping("invoke")
    public MockCallResponse invoke(@RequestBody MockCallRequest request) {
        logger.info("[MockCallCloudFacade][invoke] request:{}", JSONUtil.toJsonStr(request));
        try {
            MockCallWrapper wrapper = mockCallService.mockCall(request.getMockCallUuid(), request.getMockRequest());
            logger.info("[MockCallCloudFacade][invoke] response:{}", JSONUtil.toJsonStr(wrapper));
            return MockCallResponse.success(wrapper);
        } catch (Exception ex) {
            logger.error("[MockCallCloudFacade][invoke] invoke error:{}", ex.getLocalizedMessage());
            return MockCallResponse.fail(ex.getLocalizedMessage());
        }
    }
}
