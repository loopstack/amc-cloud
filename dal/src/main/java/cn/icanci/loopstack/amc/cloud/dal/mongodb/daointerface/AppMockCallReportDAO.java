package cn.icanci.loopstack.amc.cloud.dal.mongodb.daointerface;

import cn.icanci.loopstack.amc.cloud.dal.mongodb.dataobject.AppMockCallReportDO;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/17 23:28
 */
public interface AppMockCallReportDAO extends BaseDAO<AppMockCallReportDO> {
    /**
     * 文档对应的名字
     */
    String                     COLLECTION_NAME  = "amc-app-mock-call-report";
    /**
     * 文档对应的Class
     */
    Class<AppMockCallReportDO> COLLECTION_CLASS = AppMockCallReportDO.class;

    interface AppMockCallReportColumn extends BaseColumn {
    }
}
