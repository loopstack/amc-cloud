package cn.icanci.loopstack.amc.cloud.dal.mongodb.daointerface;

import cn.icanci.loopstack.amc.cloud.dal.mongodb.dataobject.AppMockCallDO;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 12:23
 */
public interface AppMockCallDAO extends BaseDAO<AppMockCallDO> {
    /**
     * 文档对应的名字
     */
    String               COLLECTION_NAME  = "amc-app-mock-call";
    /**
     * 文档对应的Class
     */
    Class<AppMockCallDO> COLLECTION_CLASS = AppMockCallDO.class;

    AppMockCallDO queryByAppMockName(String mockName);

    AppMockCallDO queryByUuid(String uuid);

    interface AppMockCallColumn extends BaseColumn {
        String mockName   = "mockName";
        String appUuid    = "appUuid";
        String scriptType = "scriptType";
        String script     = "script";
        String method     = "method";
    }
}
