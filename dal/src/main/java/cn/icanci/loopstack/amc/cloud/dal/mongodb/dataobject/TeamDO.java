package cn.icanci.loopstack.amc.cloud.dal.mongodb.dataobject;

/**
* @author icanci
* @since 1.0 Created in 2023/01/07 15:22
*/
public class TeamDO extends BaseDO {

    /**
     *  项目组id，唯一
     */
    private String teamId;

    /**
     * 项目组名字
     */
    private String teamName;

    /**
     * 事业群关联uuid
     */
    private String groupUuid;

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getGroupUuid() {
        return groupUuid;
    }

    public void setGroupUuid(String groupUuid) {
        this.groupUuid = groupUuid;
    }
}