package cn.icanci.loopstack.amc.cloud.dal.mongodb.dataobject;

import java.util.Date;
import java.util.StringJoiner;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 存储基本模型
 * 
 * @author icanci
 * @since 1.0 Created in 2023/01/15 11:55
 */
public class BaseDO {
    /**
     * 文档id
     */
    @Id
    private String id;

    /**
     * 雪花算法随机UUID
     */
    private String uuid;

    /**
     * 功能描述
     */
    private String desc;

    /**
     * 创建时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date   createTime;

    /**
     * 更新时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date   updateTime;

    /**
     * 状态 0有效，1无效
     */
    private int    isDelete;

    /**
     * 环境
     */
    private String env;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    @Override
    public String toString() {
        return new StringJoiner(",").add("id=" + id).add("uuid=" + uuid).add("desc=" + desc).add("createTime=" + createTime).add("updateTime=" + updateTime)
            .add("isDelete=" + isDelete).add("env=" + env).toString();
    }
}
