package cn.icanci.loopstack.amc.cloud.dal.utils.service.impl;

import cn.icanci.loopstack.amc.cloud.dal.utils.EnvUtils;
import cn.icanci.loopstack.amc.cloud.dal.utils.service.EnvService;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Service;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 08:29
 */
@Service("envService")
public class EnvServiceImpl implements EnvService, BeanPostProcessor {
    @Value("${amc.env}")
    private String env;

    @Override
    public String getEnv() {
        return env;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        EnvUtils.setEnvService(this);
        return BeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
    }
}
