package cn.icanci.loopstack.amc.cloud.dal.mongodb.dataobject;

/**
* @author icanci
* @since 1.0 Created in 2023/01/07 15:22
*/
public class GroupDO extends BaseDO {

    /**
     * 事业群组id，唯一
     */
    private String            groupId;

    /**
     * 组名字
     */
    private String            groupName;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}