package cn.icanci.loopstack.amc.cloud.dal.mongodb.daointerface;

import cn.icanci.loopstack.amc.cloud.dal.mongodb.dataobject.TeamDO;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 12:38
 */
public interface TeamDAO extends BaseDAO<TeamDO> {
    /**
     * 文档对应的名字
     */
    String        COLLECTION_NAME  = "amc-team";
    /**
     * 文档对应的Class
     */
    Class<TeamDO> COLLECTION_CLASS = TeamDO.class;

    TeamDO queryByTeamId(String teamId);

    TeamDO queryByTeamName(String teamName);

    interface TeamColumn extends BaseColumn {
        String teamId    = "teamId";
        String teamName  = "teamName";
        String groupUuid = "groupUuid";
    }
}