package cn.icanci.loopstack.amc.cloud.dal.mongodb.mongo;

import cn.icanci.loopstack.amc.cloud.dal.exception.NotSupportAccessException;
import cn.icanci.loopstack.amc.cloud.dal.mongodb.daointerface.AppMockCallReportDAO;
import cn.icanci.loopstack.amc.cloud.dal.mongodb.dataobject.AppMockCallReportDO;
import cn.icanci.loopstack.amc.common.model.PageList;

import java.util.List;

import org.springframework.stereotype.Service;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/17 23:35
 */
@Service("appMockCallReportDAO")
public class MongoAppMockCallReportDAO extends AbstractBaseDAO<AppMockCallReportDO> implements AppMockCallReportDAO {

    @Override
    public void insert(AppMockCallReportDO appMockCallReportDO) {
        super.insert(appMockCallReportDO);
        mongoTemplate.insert(appMockCallReportDO, COLLECTION_NAME);
    }

    @Override
    public void update(AppMockCallReportDO appMockCallReportDO) {
        throw new NotSupportAccessException();
    }

    @Override
    public List<AppMockCallReportDO> queryAll() {
        throw new NotSupportAccessException();
    }

    @Override
    public PageList<AppMockCallReportDO> pageQuery(AppMockCallReportDO appMockCallReportDO, int pageNum, int pageSize) {
        throw new NotSupportAccessException();
    }

    @Override
    public AppMockCallReportDO queryOneById(String _id) {
        throw new NotSupportAccessException();
    }
}
