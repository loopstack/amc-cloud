package cn.icanci.loopstack.amc.cloud.dal.mongodb.dataobject;

/**
* @author icanci
* @since 1.0 Created in 2023/01/07 15:22
*/
public class AppMockCallDO extends BaseDO {

    /**
     * mock 配置名称
     */
    private String mockName;
    /**
     * 脚本关联的uuid
     */
    private String appUuid;
    /**
     * 脚本执行类型
     */
    private String scriptType;
    /**
     * 执行的脚本内容
     */
    private String script;
    /**
     * 脚本的请求方法
     */
    private String method;

    public String getMockName() {
        return mockName;
    }

    public void setMockName(String mockName) {
        this.mockName = mockName;
    }

    public String getAppUuid() {
        return appUuid;
    }

    public void setAppUuid(String appUuid) {
        this.appUuid = appUuid;
    }

    public String getScriptType() {
        return scriptType;
    }

    public void setScriptType(String scriptType) {
        this.scriptType = scriptType;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}