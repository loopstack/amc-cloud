package cn.icanci.loopstack.amc.cloud.dal.mongodb.daointerface;

import cn.icanci.loopstack.amc.cloud.dal.mongodb.dataobject.BaseDO;
import cn.icanci.loopstack.amc.common.model.PageList;

import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 11:55
 */
public interface BaseDAO<T extends BaseDO> {

    /**
     * 插入文档一条记录
     *
     * @param t t
     */
    void insert(T t);

    /**
     * 更新文档一条记录
     *
     * @param t t
     * @return 更新结果
     */
    void update(T t);

    /**
     * 查询文档所有记录
     *
     * @return 返回查询的结果
     */
    List<T> queryAll();

    /**
     * 查询文档所有记录
     *
     * @param t 请求参数
     * @param pageNum pageNum
     * @param pageSize pageSize
     * @return 返回查询的结果
     */
    PageList<T> pageQuery(T t, int pageNum, int pageSize);

    /**
     * 根据 _id 查询一条信息
     *
     * @param _id _id
     * @return 返回查询的结果
     */
    T queryOneById(String _id);

    /** 基本表 */
    interface BaseColumn {
        /** 文档id  */
        String _id        = "_id";
        /** uuid  */
        String uuid       = "uuid";
        /** desc  */
        String desc       = "desc";
        /** createTime  */
        String createTime = "createTime";
        /** updateTime  */
        String updateTime = "updateTime";
        /** 状态 0:有效，1:无效 */
        String isDelete   = "isDelete";
        /** 操作环境  */
        String env        = "env";
    }
}
