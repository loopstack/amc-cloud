package cn.icanci.loopstack.amc.cloud.dal.utils.service.impl;

import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import cn.hutool.core.lang.Snowflake;
import cn.icanci.loopstack.amc.cloud.dal.utils.IDHolder;
import cn.icanci.loopstack.amc.cloud.dal.utils.service.IDGeneratorService;

/**
 * 分布式ID生成器
 * 
 * @author icanci
 * @since 1.0 Created in 2022/10/30 08:43
 */
@Service
public class IDGeneratorServiceImpl implements IDGeneratorService, InitializingBean {
    /** 雪花序列号生成算法 */
    private static final Snowflake SNOW_FLAKE = new Snowflake(RandomUtils.nextInt(1, 9), RandomUtils.nextInt(1, 9));

    @Override
    public String generateBySnowFlake(String prefix) {
        return prefix + SNOW_FLAKE.nextId();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        IDHolder.setIdGeneratorService(this);
    }
}
