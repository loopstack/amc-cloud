package cn.icanci.loopstack.amc.cloud.dal.mongodb.daointerface;

import cn.icanci.loopstack.amc.cloud.dal.mongodb.dataobject.LogOperateDO;
import cn.icanci.loopstack.amc.common.model.PageList;

import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 14:05
 */
public interface LogOperateDAO {
    /**
     * 文档对应的名字
     */
    String              COLLECTION_NAME  = "amc-log";
    /**
     * 文档对应的Class
     */
    Class<LogOperateDO> COLLECTION_CLASS = LogOperateDO.class;

    /**
     * 插入文档一条记录
     *
     * @param t t
     */
    void insert(LogOperateDO t);

    /**
     * 更新文档一条记录
     *
     * @param t t
     * @return 更新结果
     */
    void update(LogOperateDO t);

    /**
     * 查询文档所有记录
     *
     * @return 返回查询的结果
     */
    List<LogOperateDO> queryAll();

    /**
     * 查询文档所有记录
     *
     * @param t 请求参数
     * @param pageNum pageNum
     * @param pageSize pageSize
     * @return 返回查询的结果
     */
    PageList<LogOperateDO> pageQuery(LogOperateDO t, int pageNum, int pageSize);

    /**
     * 根据 _id 查询一条信息
     *
     * @param _id _id
     * @return 返回查询的结果
     */
    LogOperateDO queryOneById(String _id);

    interface LogColumn {
        String _id          = "id";

        /** 操作模块 */
        String module       = "module";

        /** 对象编号 */
        String targetId     = "targetId";

        /** 操作类型*/
        String operatorType = "operatorType";

        /** 操作内容 */
        String content      = "content";

        /** 创建时间 */
        String createTime   = "createTime";

        /** 环境 */
        String env          = "env";
    }
}
