package cn.icanci.loopstack.amc.cloud.dal.utils.service;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 08:28
 */
public interface EnvService {
    /**
     * 获取当前的环境信息
     * 
     * @return 返回当前的环境信息
     */
    String getEnv();
}
