package cn.icanci.loopstack.amc.cloud.dal.utils.service;

/**
 * @author icanci
 * @since 1.0 Created in 2022/10/30 08:43
 */
public interface IDGeneratorService {
    /**
     * 生成唯一id
     *
     * @param prefix 前缀
     * @return id
     */
    String generateBySnowFlake(String prefix);
}
