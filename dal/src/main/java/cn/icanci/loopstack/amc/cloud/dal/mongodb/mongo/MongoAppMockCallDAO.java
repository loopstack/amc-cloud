package cn.icanci.loopstack.amc.cloud.dal.mongodb.mongo;

import cn.icanci.loopstack.amc.cloud.dal.mongodb.daointerface.AppMockCallDAO;
import cn.icanci.loopstack.amc.cloud.dal.mongodb.dataobject.AppMockCallDO;
import cn.icanci.loopstack.amc.common.model.PageList;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 12:24
 */
@Service("appMockCallDAO")
public class MongoAppMockCallDAO extends AbstractBaseDAO<AppMockCallDO> implements AppMockCallDAO {

    @Override
    public void insert(AppMockCallDO appMockCallDO) {
        super.insert(appMockCallDO);
        mongoTemplate.insert(appMockCallDO, COLLECTION_NAME);
    }

    @Override
    public void update(AppMockCallDO appMockCallDO) {
        super.update(appMockCallDO);
        mongoTemplate.save(appMockCallDO, COLLECTION_NAME);
    }

    @Override
    public List<AppMockCallDO> queryAll() {
        Criteria criteria = Criteria.where(AppMockCallColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.find(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public PageList<AppMockCallDO> pageQuery(AppMockCallDO appMockCallDO, int pageNum, int pageSize) {
        Criteria criteria = Criteria.where(AppMockCallColumn.env).is(DEFAULT_ENV);

        if (StringUtils.isNotBlank(appMockCallDO.getMockName())) {
            // 不分区大小写查询，其中操作符"i"：表示不分区大小写
            criteria.and(AppMockCallColumn.mockName).regex("^.*" + appMockCallDO.getMockName() + ".*$", "i");
        }

        Query query = new Query(criteria);
        query.with(Sort.by(Sort.Direction.DESC, AppMockCallColumn.createTime));

        return pageQuery(query, COLLECTION_CLASS, pageSize, pageNum, COLLECTION_NAME);
    }

    @Override
    public AppMockCallDO queryOneById(String _id) {
        Criteria criteria = Criteria.where(AppMockCallColumn._id).is(_id);
        criteria.and(AppMockCallColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public AppMockCallDO queryByAppMockName(String mockName) {
        Criteria criteria = Criteria.where(AppMockCallColumn.mockName).is(mockName);
        criteria.and(AppMockCallColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public AppMockCallDO queryByUuid(String uuid) {
        Criteria criteria = Criteria.where(AppMockCallColumn.uuid).is(uuid);
        criteria.and(AppMockCallColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }
}
