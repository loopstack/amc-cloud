package cn.icanci.loopstack.amc.cloud.dal.mongodb.dataobject;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/17 23:29
 */
public class AppMockCallReportDO extends BaseDO {
    /**
     * mock 过程是否成功
     */
    private boolean success;
    /**
     * mockName
     */
    private String  mockName;
    /**
     * mockUuid
     */
    private String  mockUuid;
    /**
     * mock执行的请求
     */
    private String  mockRequest;
    /**
     * mock执行时间
     */
    private long    runtime;
    /**
     * mock执行的异常信息
     */
    private String  mockErrorMessage;
    /**
     * 真正mock返回的结果
     */
    private String  mockResponse;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMockName() {
        return mockName;
    }

    public void setMockName(String mockName) {
        this.mockName = mockName;
    }

    public String getMockUuid() {
        return mockUuid;
    }

    public void setMockUuid(String mockUuid) {
        this.mockUuid = mockUuid;
    }

    public String getMockRequest() {
        return mockRequest;
    }

    public void setMockRequest(String mockRequest) {
        this.mockRequest = mockRequest;
    }

    public long getRuntime() {
        return runtime;
    }

    public void setRuntime(long runtime) {
        this.runtime = runtime;
    }

    public String getMockErrorMessage() {
        return mockErrorMessage;
    }

    public void setMockErrorMessage(String mockErrorMessage) {
        this.mockErrorMessage = mockErrorMessage;
    }

    public String getMockResponse() {
        return mockResponse;
    }

    public void setMockResponse(String mockResponse) {
        this.mockResponse = mockResponse;
    }
}
