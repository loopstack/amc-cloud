package cn.icanci.loopstack.amc.cloud.dal.mongodb.daointerface;

import cn.icanci.loopstack.amc.cloud.dal.mongodb.dataobject.AppDO;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 12:31
 */
public interface AppDAO extends BaseDAO<AppDO> {
    /**
     * 文档对应的名字
     */
    String       COLLECTION_NAME  = "amc-app";
    /**
     * 文档对应的Class
     */
    Class<AppDO> COLLECTION_CLASS = AppDO.class;

    AppDO queryByAppId(String appId);

    AppDO queryByAppName(String appName);

    interface AppColumn extends BaseColumn {
        String appId    = "appId";
        String appName  = "appName";
        String teamUuid = "teamUuid";
    }
}