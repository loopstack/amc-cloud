package cn.icanci.loopstack.amc.cloud.dal.mongodb.dataobject;

import cn.icanci.loopstack.amc.common.enums.LogOperatorTypeEnum;

import java.util.Date;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 13:39
 */
public class LogOperateDO {
    /** 编号 */
    @Id
    private String id;

    /** 操作模块 */
    private String module;

    /** 对象编号 */
    private String targetId;

    /** 
     * 操作类型
     * 
     * @see LogOperatorTypeEnum#name()
     */
    private String operatorType;

    /** 操作内容 */
    private String content;

    /** 创建时间 */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date   createTime;

    /** 环境 */
    private String env;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String getOperatorType() {
        return operatorType;
    }

    public void setOperatorType(String operatorType) {
        this.operatorType = operatorType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }
}
