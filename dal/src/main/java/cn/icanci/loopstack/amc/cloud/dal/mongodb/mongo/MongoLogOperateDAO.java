package cn.icanci.loopstack.amc.cloud.dal.mongodb.mongo;

import cn.icanci.loopstack.amc.cloud.dal.mongodb.daointerface.LogOperateDAO;
import cn.icanci.loopstack.amc.cloud.dal.mongodb.dataobject.LogOperateDO;
import cn.icanci.loopstack.amc.cloud.dal.utils.EnvUtils;
import cn.icanci.loopstack.amc.common.model.PageList;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 * @author icanci
 * @since 1.0 Created in 2022/11/11 16:06
 */
@Service("logOperateDAO")
public class MongoLogOperateDAO extends MongoPageHelper implements LogOperateDAO {
    @Override
    public void insert(LogOperateDO t) {
        t.setId(null);
        t.setEnv(EnvUtils.getEnv());
        mongoTemplate.insert(t, COLLECTION_NAME);
    }

    @Override
    public void update(LogOperateDO t) {
        throw new RuntimeException("Not Support Update!");
    }

    @Override
    public List<LogOperateDO> queryAll() {
        Criteria criteria = Criteria.where(LogColumn.env).is(EnvUtils.getEnv());
        Query query = new Query(criteria);
        return mongoTemplate.find(query, COLLECTION_CLASS, COLLECTION_NAME);

    }

    @Override
    public PageList<LogOperateDO> pageQuery(LogOperateDO t, int pageNum, int pageSize) {
        Criteria criteria = Criteria.where(LogColumn.env).is(EnvUtils.getEnv());
        criteria.and(LogColumn.module).is(t.getModule());
        criteria.and(LogColumn.targetId).is(t.getTargetId());

        Query query = new Query(criteria);
        query.with(Sort.by(Sort.Direction.DESC, LogColumn.createTime));

        return pageQuery(query, COLLECTION_CLASS, pageSize, pageNum, COLLECTION_NAME);

    }

    @Override
    public LogOperateDO queryOneById(String _id) {
        Criteria criteria = Criteria.where(LogColumn._id).is(_id);
        criteria.and(LogColumn.env).is(EnvUtils.getEnv());
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }
}
