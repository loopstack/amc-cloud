package cn.icanci.loopstack.amc.cloud.dal.exception;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/17 23:37
 */
public class NotSupportAccessException extends RuntimeException {
    public NotSupportAccessException() {
        super();
    }

    public NotSupportAccessException(String message) {
        super(message);
    }

    public NotSupportAccessException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotSupportAccessException(Throwable cause) {
        super(cause);
    }

    protected NotSupportAccessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
