package cn.icanci.loopstack.amc.cloud.dal.mongodb.dataobject;

/**
* @author icanci
* @since 1.0 Created in 2023/01/07 15:22
*/
public class AppDO extends BaseDO {

    /**
     * 项目id，全局唯一
     */
    private String appId;

    /**
     * 项目名字
     */
    private String appName;

    /**
     * 项目组关联uuid
     */
    private String teamUuid;

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppId() {
        return this.appId;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppName() {
        return this.appName;
    }

    public void setTeamUuid(String teamUuid) {
        this.teamUuid = teamUuid;
    }

    public String getTeamUuid() {
        return this.teamUuid;
    }
}