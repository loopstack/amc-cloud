package cn.icanci.loopstack.amc.cloud.dal.mongodb.daointerface;

import cn.icanci.loopstack.amc.cloud.dal.mongodb.dataobject.GroupDO;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 12:31
 */
public interface GroupDAO extends BaseDAO<GroupDO> {
    /**
     * 文档对应的名字
     */
    String         COLLECTION_NAME  = "amc-group";
    /**
     * 文档对应的Class
     */
    Class<GroupDO> COLLECTION_CLASS = GroupDO.class;

    GroupDO queryByGroupId(String groupId);

    GroupDO queryByGroupName(String groupName);

    interface GroupColumn extends BaseColumn {
        String groupId   = "groupId";
        String groupName = "groupName";
    }
}