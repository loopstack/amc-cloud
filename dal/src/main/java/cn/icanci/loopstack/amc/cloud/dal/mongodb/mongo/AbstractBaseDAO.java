package cn.icanci.loopstack.amc.cloud.dal.mongodb.mongo;

import cn.icanci.loopstack.amc.cloud.dal.mongodb.daointerface.BaseDAO;
import cn.icanci.loopstack.amc.cloud.dal.mongodb.dataobject.BaseDO;
import cn.icanci.loopstack.amc.cloud.dal.utils.EnvUtils;
import cn.icanci.loopstack.amc.cloud.dal.utils.IDHolder;

import java.util.Date;

import org.springframework.beans.factory.InitializingBean;

/**
 * @author icanci
 * @since 1.0 Created in 2022/10/24 22:47
 */
public abstract class AbstractBaseDAO<T extends BaseDO> extends MongoPageHelper implements BaseDAO<T>, InitializingBean {

    protected String DEFAULT_ENV;

    @Override
    public void insert(T t) {
        // 处理插入数据
        t.setId(null);
        t.setIsDelete(0);
        t.setCreateTime(new Date());
        t.setUpdateTime(new Date());
        t.setEnv(DEFAULT_ENV);
        t.setUuid(IDHolder.generateNoBySnowFlakeDefaultPrefix());
    }

    @Override
    public void update(T t) {
        t.setUpdateTime(new Date());
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        DEFAULT_ENV = EnvUtils.getEnv();
    }
}
