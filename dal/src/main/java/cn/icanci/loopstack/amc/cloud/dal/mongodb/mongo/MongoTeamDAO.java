package cn.icanci.loopstack.amc.cloud.dal.mongodb.mongo;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import cn.icanci.loopstack.amc.cloud.dal.mongodb.daointerface.TeamDAO;
import cn.icanci.loopstack.amc.cloud.dal.mongodb.dataobject.TeamDO;
import cn.icanci.loopstack.amc.common.model.PageList;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/15 12:24
 */
@Service("teamDAO")
public class MongoTeamDAO extends AbstractBaseDAO<TeamDO> implements TeamDAO {

    @Override
    public void insert(TeamDO team) {
        super.insert(team);
        mongoTemplate.insert(team, COLLECTION_NAME);
    }

    @Override
    public void update(TeamDO team) {
        super.update(team);
        mongoTemplate.save(team, COLLECTION_NAME);
    }

    @Override
    public List<TeamDO> queryAll() {
        Criteria criteria = Criteria.where(TeamColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.find(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public PageList<TeamDO> pageQuery(TeamDO team, int pageNum, int pageSize) {
        Criteria criteria = Criteria.where(TeamColumn.env).is(DEFAULT_ENV);

        if (StringUtils.isNotBlank(team.getTeamName())) {
            // 不分区大小写查询，其中操作符"i"：表示不分区大小写
            criteria.and(TeamColumn.teamName).regex("^.*" + team.getTeamName() + ".*$", "i");
        }

        if (StringUtils.isNotBlank(team.getTeamId())) {
            criteria.and(TeamColumn.teamId).is(team.getTeamId());
        }

        if (StringUtils.isNotBlank(team.getGroupUuid())) {
            criteria.and(TeamColumn.groupUuid).is(team.getGroupUuid());
        }

        Query query = new Query(criteria);
        query.with(Sort.by(Sort.Direction.DESC, TeamColumn.createTime));

        return pageQuery(query, COLLECTION_CLASS, pageSize, pageNum, COLLECTION_NAME);
    }

    @Override
    public TeamDO queryOneById(String _id) {
        Criteria criteria = Criteria.where(TeamColumn._id).is(_id);
        criteria.and(TeamColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public TeamDO queryByTeamId(String teamId) {
        Criteria criteria = Criteria.where(TeamColumn.teamId).is(teamId);
        criteria.and(TeamColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }

    @Override
    public TeamDO queryByTeamName(String teamName) {
        Criteria criteria = Criteria.where(TeamColumn.teamName).is(teamName);
        criteria.and(TeamColumn.env).is(DEFAULT_ENV);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, COLLECTION_CLASS, COLLECTION_NAME);
    }
}
