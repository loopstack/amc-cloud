package cn.icanci.loopstack.amc.cloud.dal.utils;

import cn.icanci.loopstack.amc.cloud.dal.utils.service.IDGeneratorService;

/**
 * Id 生成器
 * 
 * @author icanci
 * @since 1.0 Created in 2022/11/11 14:18
 */
public class IDHolder {
    /** 分布式id服务 */
    private static IDGeneratorService idGeneratorService;

    private static final String       DEFAULT_PREFIX = "AMC";

    public static void setIdGeneratorService(IDGeneratorService idGeneratorService) {
        IDHolder.idGeneratorService = idGeneratorService;
    }

    /**
     * 通过雪花算法生成唯一id
     *
     * @param prefix 前缀
     * @return id
     */
    public static String generateNoBySnowFlake(String prefix) {
        return idGeneratorService.generateBySnowFlake(prefix);
    }

    /**
     * 通过雪花算法生成唯一id，默认 REC
     *
     * @return id
     */
    public static String generateNoBySnowFlakeDefaultPrefix() {
        return idGeneratorService.generateBySnowFlake(DEFAULT_PREFIX);
    }

}
