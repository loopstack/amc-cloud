package cn.icanci.loopstack.amc.cloud.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/16 15:25
 */
@ComponentScan(basePackages = { "cn.icanci.loopstack.amc.cloud", "cn.icanci.loopstack.amc.admin" })
@SpringBootApplication
public class MockCloudApplication {
    public static void main(String[] args) {
        SpringApplication.run(MockCloudApplication.class, args);
    }
}
