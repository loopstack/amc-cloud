package cn.icanci.loopstack.amc.cloud.common.utils.test;

import cn.icanci.loopstack.amc.cloud.common.utils.WrapperUtils;

import org.junit.Test;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/17 11:19
 */
public class WrapperUtilsTest {
    @Test
    public void testBoolean() {
        Boolean b = Boolean.TRUE;
        Boolean aBoolean = WrapperUtils.toBoolean("FAlse");
        System.out.println(aBoolean);
    }
}
