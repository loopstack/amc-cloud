package cn.icanci.loopstack.amc.cloud.common.response;

import cn.icanci.loopstack.amc.cloud.common.model.MockCallWrapper;

import java.io.Serializable;

/**
 * MockFacade执行请求返回结果
 *
 * @author icanci
 * @since 1.0 Created in 2023/01/16 15:20
 */
public class MockCallResponse implements Serializable {

    /**
     * 是否成功
     */
    private boolean         success;
    /**
     * 请求调用错误信息
     */
    private String          errorMessage;
    /**
     * Mock Wrapper
     */
    private MockCallWrapper wrapper;

    public static MockCallResponse success(MockCallWrapper wrapper) {
        MockCallResponse response = new MockCallResponse();
        response.setSuccess(true);
        response.setWrapper(wrapper);
        return response;
    }

    public static MockCallResponse fail(String localizedMessage) {
        MockCallResponse response = new MockCallResponse();
        response.setSuccess(true);
        response.setErrorMessage(localizedMessage);
        return response;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public MockCallWrapper getWrapper() {
        return wrapper;
    }

    public void setWrapper(MockCallWrapper wrapper) {
        this.wrapper = wrapper;
    }
}
