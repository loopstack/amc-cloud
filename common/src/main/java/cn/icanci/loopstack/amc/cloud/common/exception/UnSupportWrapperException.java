package cn.icanci.loopstack.amc.cloud.common.exception;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/17 11:16
 */
public class UnSupportWrapperException extends RuntimeException {
    public UnSupportWrapperException() {
        super();
    }

    public UnSupportWrapperException(String message) {
        super(message);
    }

    public UnSupportWrapperException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnSupportWrapperException(Throwable cause) {
        super(cause);
    }

    protected UnSupportWrapperException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
