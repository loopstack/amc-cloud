package cn.icanci.loopstack.amc.cloud.common.request;

import java.io.Serializable;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/16 15:19
 */
public class MockCallRequest implements Serializable {
    /**
     * Mock 的链路id
     */
    private String traceId;
    /**
     * 需要执行的Mock
     */
    private String mockCallUuid;
    /**
     * 需要处理的请求数据
     */
    private String mockRequest;

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getMockCallUuid() {
        return mockCallUuid;
    }

    public void setMockCallUuid(String mockCallUuid) {
        this.mockCallUuid = mockCallUuid;
    }

    public String getMockRequest() {
        return mockRequest;
    }

    public void setMockRequest(String mockRequest) {
        this.mockRequest = mockRequest;
    }
}
