package cn.icanci.loopstack.amc.cloud.common.model;

import java.io.Serializable;

/**
 * MockFacade执行请求返回结果
 *
 * @author icanci
 * @since 1.0 Created in 2023/01/16 15:20
 */
public class MockCallWrapper implements Serializable {
    /**
     * mock 过程是否成功
     */
    private boolean success;
    /**
     * mockName
     */
    private String  mockName;
    /**
     * mockUuid
     */
    private String  mockUuid;
    /**
     * mock执行的请求
     */
    private String  mockRequest;
    /**
     * mock执行的异常信息
     */
    private String  mockErrorMessage;
    /**
     * 真正mock返回的结果
     */
    private Object  mockResponse;

    /**
     * 失败
     *
     * @param mockUuid mockUuid
     * @param message message
     * @return Wrapper
     */
    public static MockCallWrapper fail(String mockUuid, String message) {
        MockCallWrapper wrapper = new MockCallWrapper();
        wrapper.setSuccess(false);
        wrapper.setMockUuid(mockUuid);
        wrapper.setMockErrorMessage(message);
        return wrapper;
    }

    /**
     * 失败
     *
     * @param mockUuid mockUuid
     * @param mockName mockName
     * @param mockRequest mockRequest
     * @param message message
     * @return 返回结果
     */
    public static MockCallWrapper fail(String mockUuid, String mockName, String mockRequest, String message) {
        MockCallWrapper wrapper = new MockCallWrapper();
        wrapper.setSuccess(false);
        wrapper.setMockName(mockName);
        wrapper.setMockUuid(mockUuid);
        wrapper.setMockRequest(mockRequest);
        wrapper.setMockErrorMessage(message);
        return wrapper;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMockName() {
        return mockName;
    }

    public void setMockName(String mockName) {
        this.mockName = mockName;
    }

    public String getMockUuid() {
        return mockUuid;
    }

    public void setMockUuid(String mockUuid) {
        this.mockUuid = mockUuid;
    }

    public String getMockRequest() {
        return mockRequest;
    }

    public void setMockRequest(String mockRequest) {
        this.mockRequest = mockRequest;
    }

    public String getMockErrorMessage() {
        return mockErrorMessage;
    }

    public void setMockErrorMessage(String mockErrorMessage) {
        this.mockErrorMessage = mockErrorMessage;
    }

    public Object getMockResponse() {
        return mockResponse;
    }

    public void setMockResponse(Object mockResponse) {
        this.mockResponse = mockResponse;
    }
}
