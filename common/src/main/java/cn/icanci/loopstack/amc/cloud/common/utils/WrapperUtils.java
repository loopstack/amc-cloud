package cn.icanci.loopstack.amc.cloud.common.utils;

import cn.hutool.json.JSONUtil;
import cn.icanci.loopstack.amc.cloud.common.exception.UnSupportWrapperException;

import java.math.BigDecimal;

/**
 * @author icanci
 * @since 1.0 Created in 2023/01/17 11:10
 */
public class WrapperUtils {
    private WrapperUtils() {
    }

    /**
     * 转换为bean
     * 
     * @param wrapperResp wrapperResp
     * @param clazz clazz
     * @param <T> T 类型
     * @return 返回指定对象数据
     */
    public static <T> T toObjectBean(Object wrapperResp, Class<T> clazz) {
        try {
            return JSONUtil.toBean(JSONUtil.toJsonStr(wrapperResp), clazz);
        } catch (Exception ex) {
            throw new UnSupportWrapperException(ex);
        }
    }

    /**
     * toBoolean
     * 
     * @param wrapperResp wrapperResp
     * @return toBoolean
     */
    public static Boolean toBoolean(Object wrapperResp) {
        try {
            if (wrapperResp == null) {
                return null;
            }
            if (wrapperResp instanceof Boolean) {
                return (Boolean) wrapperResp;
            }
            String resp = wrapperResp.toString();
            if (Boolean.TRUE.toString().equalsIgnoreCase(resp) || Boolean.FALSE.toString().equalsIgnoreCase(resp)) {
                return Boolean.valueOf(resp);
            }
            throw new UnSupportWrapperException("UnSupport parse " + wrapperResp + " toBoolean");
        } catch (Exception ex) {
            throw new UnSupportWrapperException(ex);
        }
    }

    /**
     * toBooleanUnBox
     * 
     * @param wrapperResp wrapperResp
     * @return toBooleanUnBox
     */
    public static boolean toBooleanUnBox(Object wrapperResp) {
        try {
            if (wrapperResp == null) {
                throw new UnSupportWrapperException("UnSupport parse null toBooleanUnBox");
            }
            if (wrapperResp instanceof Boolean) {
                return (Boolean) wrapperResp;
            }
            throw new UnSupportWrapperException("UnSupport parse " + wrapperResp + " toBoolean");
        } catch (Exception ex) {
            throw new UnSupportWrapperException(ex);
        }
    }

    /**
     * toInteger
     *
     * @param wrapperResp wrapperResp
     * @return toInteger
     */
    public static Integer toInteger(Object wrapperResp) {
        try {
            if (wrapperResp == null) {
                return null;
            }
            if (wrapperResp instanceof Integer) {
                return (Integer) wrapperResp;
            }
            throw new UnSupportWrapperException("UnSupport parse " + wrapperResp + " toInteger");
        } catch (Exception ex) {
            throw new UnSupportWrapperException(ex);
        }
    }

    /**
     * toIntegerUnBox
     *
     * @param wrapperResp wrapperResp
     * @return toIntegerUnBox
     */
    public static int toIntegerUnBox(Object wrapperResp) {
        try {
            if (wrapperResp == null) {
                throw new UnSupportWrapperException("UnSupport parse null toIntegerUnBox");
            }
            if (wrapperResp instanceof Integer) {
                return (Integer) wrapperResp;
            }
            throw new UnSupportWrapperException("UnSupport parse " + wrapperResp + " toIntegerUnBox");
        } catch (Exception ex) {
            throw new UnSupportWrapperException(ex);
        }
    }

    /**
     * toLong
     *
     * @param wrapperResp wrapperResp
     * @return toLong
     */
    public static Long toLong(Object wrapperResp) {
        try {
            if (wrapperResp == null) {
                return null;
            }
            if (wrapperResp instanceof Long) {
                return (Long) wrapperResp;
            }
            throw new UnSupportWrapperException("UnSupport parse " + wrapperResp + " toLong");
        } catch (Exception ex) {
            throw new UnSupportWrapperException(ex);
        }
    }

    /**
     * toLongUnBox
     *
     * @param wrapperResp wrapperResp
     * @return toLongUnBox
     */
    public static long toLongUnBox(Object wrapperResp) {
        try {
            if (wrapperResp == null) {
                throw new UnSupportWrapperException("UnSupport parse null toLongUnBox");
            }
            if (wrapperResp instanceof Long) {
                return (Long) wrapperResp;
            }
            throw new UnSupportWrapperException("UnSupport parse " + wrapperResp + " toLongUnBox");
        } catch (Exception ex) {
            throw new UnSupportWrapperException(ex);
        }
    }

    /**
     * toDouble
     *
     * @param wrapperResp wrapperResp
     * @return toDouble
     */
    public static Double toDouble(Object wrapperResp) {
        try {
            if (wrapperResp == null) {
                return null;
            }
            if (wrapperResp instanceof Double) {
                return (Double) wrapperResp;
            }
            throw new UnSupportWrapperException("UnSupport parse " + wrapperResp + " toDouble");
        } catch (Exception ex) {
            throw new UnSupportWrapperException(ex);
        }
    }

    /**
     * toDoubleUnBox
     *
     * @param wrapperResp wrapperResp
     * @return toDoubleUnBox
     */
    public static double toDoubleUnBox(Object wrapperResp) {
        try {
            if (wrapperResp == null) {
                throw new UnSupportWrapperException("UnSupport parse null toDoubleUnBox");
            }
            if (wrapperResp instanceof Double) {
                return (Double) wrapperResp;
            }
            throw new UnSupportWrapperException("UnSupport parse " + wrapperResp + " toDoubleUnBox");
        } catch (Exception ex) {
            throw new UnSupportWrapperException(ex);
        }
    }

    /**
     * toFloat
     *
     * @param wrapperResp wrapperResp
     * @return toFloat
     */
    public static Float toFloat(Object wrapperResp) {
        try {
            if (wrapperResp == null) {
                return null;
            }
            if (wrapperResp instanceof Float) {
                return (Float) wrapperResp;
            }
            throw new UnSupportWrapperException("UnSupport parse " + wrapperResp + " toFloat");
        } catch (Exception ex) {
            throw new UnSupportWrapperException(ex);
        }
    }

    /**
     * toFloatUnBox
     *
     * @param wrapperResp wrapperResp
     * @return toFloatUnBox
     */
    public static float toFloatUnBox(Object wrapperResp) {
        try {
            if (wrapperResp == null) {
                throw new UnSupportWrapperException("UnSupport parse null toFloatUnBox");
            }
            if (wrapperResp instanceof Float) {
                return (Float) wrapperResp;
            }
            throw new UnSupportWrapperException("UnSupport parse " + wrapperResp + " toFloatUnBox");
        } catch (Exception ex) {
            throw new UnSupportWrapperException(ex);
        }
    }

    /**
     * toBigDecimal
     *
     * @param wrapperResp wrapperResp
     * @return toBigDecimal
     */
    public static BigDecimal toBigDecimal(Object wrapperResp) {
        try {
            if (wrapperResp == null) {
                return null;
            }
            return new BigDecimal(String.valueOf(wrapperResp));
        } catch (Exception ex) {
            throw new UnSupportWrapperException(ex);
        }
    }

    /**
     * toStringValue
     * 
     * @param wrapperResp wrapperResp
     * @return toStringValue
     */
    public static String toStringValue(Object wrapperResp) {
        try {
            if (wrapperResp == null) {
                return null;
            }
            return wrapperResp.toString();
        } catch (Exception ex) {
            throw new UnSupportWrapperException(ex);
        }
    }
}
